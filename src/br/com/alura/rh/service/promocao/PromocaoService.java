package br.com.alura.rh.service.promocao;

import br.com.alura.rh.ValidacaoException;
import br.com.alura.rh.model.Cargo;
import br.com.alura.rh.model.Funcionario;

public class PromocaoService {

    public void promover(Funcionario funcionario, boolean metaBatida) {
        Cargo cargoAtual = funcionario.getDadosPessoais().getCargo();
        if(Cargo.GERENTE == cargoAtual) {
            throw new ValidacaoException("Gerentes não podem ser promovidos");
        }
        
        if(metaBatida) {
            Cargo cargo = cargoAtual.getProximoCargo();
            funcionario.promover(cargo);
        } else {
            throw new ValidacaoException("Funcionario nao bateu a meta");
        }
    }

}
